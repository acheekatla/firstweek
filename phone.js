function valid(num) {
  let numberformat = /^0?[6-9]\d{9}$/; //number format

  if (numberformat.test(num)) {
    return "It is a valid phone number";
  } else {
    return "It is invalid phone number";
  }
}

console.log(valid(9492100467));

function fact(num) {
  if (num === 0 || num === 1) {
    return 1;
  }
  for (var i = num - 1; i > 1; i--) {
    num = num * i;
  }
  return num;
}
fact(5);
console.log(fact(5));

function combi(n, r) {
  var ncr = fact(n) / (fact(n - r) * fact(r));
  return ncr;
}
combi(5, 3);
console.log(combi(5, 3));

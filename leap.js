function isLeapYear(year) {
  var year
  if ((0 == year % 4 && 0 != year % 100) || 0 == year % 400) {
    return true
  } else {
    return false
  }
}

var yr = parseInt(process.argv[2])
console.log("The year " + yr + " is " + (isLeapYear(yr) ? "" : "not ") + "a leap year.")
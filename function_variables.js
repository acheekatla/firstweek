function val(){

		var value= 9; 
		console.log(value) ;															// int type

		let age = 6; 
		console.log(age);																//int type

		let vehicle='car';
		console.log(vehicle);															// string type

		const price=85.8; 
		console.log(price);																//float type

		let data = {firstName:"John", lastName:"Doe"};									// Object type
		console.log(data);

		const cars = ["Saab", "toyota", "BMW"];	
		console.log(cars)	;															//array

		
		const number = 89;																//number
		console.log(number);	
															
}

val();